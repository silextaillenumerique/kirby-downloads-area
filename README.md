# Kirby downloads area

Set up a secured downloads area on your Kirby website. The area will be accessible from the front end by guest visitors.

## 1. Installation

Download and copy this repository to `/site/plugins/downloads-area`

Alternatively, you can install it with composer: `composer require silextaillenumerique/downloads-area`.



## 2. Setup

The plugin will automatically create a dedicated page in your `content` directory. The page default ID is `"downloads"` but you can define the ID of your choice with the `silextaillenumerique.downloads-area.root` option in your `config.php` file.



## 3. Usage

The plugin adds a dedicated page in the panel. In this page it is possible to create one or more downloads spaces. Each space can contain multiple sections and each sections can contain multiple files.

### Spaces

If there is more than one space, each space is a page with its own url. It is restrain access to a space to a specific user. 

### Sections

On save, a zip file is generated with all the files of the section so it is possible to download them all at once. 

### Guest users

You can add users with the `guest` role from the users page in the panel. Guests will be able to login with the password you will give them in order to access to their space(s) but they wont be able to acces to the panel.



## 4. Customize

The plugin comes with its own rules of CSS. You can add a link to your own CSS file using option `silextaillenumerique.downloads-area.custom-css`.



## 5. License

MIT



