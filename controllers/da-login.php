<?php

return function ($kirby) {

    // don't show the login screen to already logged in users
    if ($kirby->user()) {
        go($kirby->request()->url());
    }

    $error = false;
    $error_type = "";

    // handle the form submission
    if ($kirby->request()->is('POST')) {

        // fetch the user by username
        if ($user = $kirby->user(get('user'))) {
            // if the user exists, try to log them in
            try {
                $user->login(get('password'));
                // redirect to the homepage
                // if the login was successful
                go($kirby->request()->url());
            } catch (Exception $e) {
                $error = true;
                $error_type = "wrong_credentials";
            }
        } else {
            // make sure the alert is
            // displayed in the template
            $error = true;
            $error_type = "wrong_credentials";
        }
    }

    return [
        'error' => $error,
        'error_type' => $error_type
    ];
};
