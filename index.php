<?php
Kirby::plugin('silextaillenumerique/downloads-area', [
    'options' => [
        'root' => 'downloads',
        'custom-css' => '',
        'custom-js' => '',
        'css' => [
            'typography' => true,
            'layout' => true,
            'inputs' => true
        ],
        'header' => 'da-page-header',
        'footer' => 'da-page-footer',
    ],
    'blueprints' => [
        'pages/da-home' => __DIR__ . '/blueprints/pages/da-home.yml',
        'pages/da-space' => __DIR__ . '/blueprints/pages/da-space.yml',
        'pages/da-section' => __DIR__ . '/blueprints/pages/da-section.yml',
        'users/da-guest' => __DIR__ . '/blueprints/users/da-guest.yml',
        'users/da-guest' => __DIR__ . '/blueprints/users/da-guest.yml',
        'files/da-file' => __DIR__ . '/blueprints/files/da-file.yml',
    ],
    'templates' => [
        'da-home' => __DIR__ . '/templates/da-home.php',
        'da-login' => __DIR__ . '/templates/da-login.php',
        'da-space' => __DIR__ . '/templates/da-space.php',
        'da-section' => __DIR__ . '/templates/da-section.php',
    ],
    'snippets' => [
        'da-header' => __DIR__ . '/snippets/da-header.php',
        'da-footer' => __DIR__ . '/snippets/da-footer.php',
        'da-page-header' => __DIR__ . '/snippets/da-page-header.php',
        'da-page-footer' => __DIR__ . '/snippets/da-page-footer.php',
        'da-section' => __DIR__ . '/snippets/da-section.php',
        'da-files-grid' => __DIR__ . '/snippets/da-files-grid.php',
    ],
    'controllers' => [
        'da-login' => require 'controllers/da-login.php',
    ],
    'routes' => [
        [
            'pattern' => 'logout',
            'action'  => function () {
                if ($user = kirby()->user()) {
                    $user->logout();
                }
                go('/');
            }
        ],
        [
            'pattern' => option('silextaillenumerique.downloads-area.root') . '/download',
            'method' => 'POST',
            'action' => function () {
                $data = kirby()->request()->data();
                try {
                    $section = kirby()->page($data['section']);
                    $archive = $section->getArchive();
                    if ($archive) {
                        return Response::download($archive->root(), $archive->filename());
                    } else {
                        throw new Exception('Missing archive');
                    }
                } catch (Exception $e) {
                    $response = [
                        'error' => 'true',
                        'error_type' => 'download',
                        //'error_message' => $e->getMessage()
                    ];
                    return go($data['page'] . '?' . http_build_query($response));
                }
            }
        ],
        [
            'pattern' => option('silextaillenumerique.downloads-area.root').'(:all)',
            'method' => 'GET|POST',
            'action'  => function ($all) {
                $all = ltrim($all, '/');
                if (!kirby()->user()) {
                    return new Page([
                      'slug' => 'da-login',
                      'template' => 'da-login',
                      'content' => [
                        'title' => kirby()->page(option('silextaillenumerique.downloads-area.root'))->title()
                      ]
                    ]);
                } 
                $spaces = kirby()->site()->getUserSpaces();
                if (!$all && $spaces->count() == 1 && $space = $spaces->first()) {
                    return go($space);
                } else {
                    $this->next();
                }
            }
        ],
    ],
    'siteMethods' => [
        'getUserSpaces' => function () {
            $user = kirby()->user();
            $spaces = page(option('silextaillenumerique.downloads-area.root'))->children()->listed()->template('da-space');
            if ($user && $user->role() != 'admin') {
                $spaces = $spaces->filterBy('id', 'in', $user->allowedSpaces()->yaml());
            }
            return $spaces;
        },
        'formatBytes' => function ($bytes, $precision = 2) {
            $units = array('B', 'KB', 'MB', 'GB', 'TB');

            $bytes = max($bytes, 0);
            $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
            $pow = min($pow, count($units) - 1);
            $bytes /= pow(1024, $pow);

            return round($bytes, $precision) . ' ' . $units[$pow];
        }
    ],
    'pageMethods' => [
        'getArchiveName' => function () {
            return str_replace('/', '_', $this->id() . '.zip');
        },
        'getArchivePath' => function () {
            $archives = kirby()->page(option('silextaillenumerique.downloads-area.root'));
            return $archives->root() . '/' . $this->getArchiveName();
        },
        'getArchive' => function () {
            $archives = kirby()->page(option('silextaillenumerique.downloads-area.root'));
            $archiveName = $this->getArchiveName();
            return $archives->file($archiveName) && $archives->file($archiveName)->exists() ? $archives->file($archiveName) : null;
        },
        'removeArchive' => function () {
            if ($this->getArchive()) {
                $this->getArchive()->delete();
            }
        },
        'setSectionArchive' => function(){
            if ($this->intendedTemplate() == 'da-section') {
                $this->setArchive();
            }
        },
        'removeSectionArchive' => function(){
            if ($this->intendedTemplate() == 'da-section') {
                $this->removeArchive();
            }
        },
        'setArchive' => function () {
            $this->removeArchive();
            if ($this->selection()->isNotEmpty() && $files = $this->selection()->toFiles()) {
                $zip = new ZipArchive();
                if ($zip->open($this->getArchivePath(), ZIPARCHIVE::CREATE) === true) {
                    foreach ($files as $file) {
                        if ($file && strlen($file->filename())) {
                            $zip->addFile(realpath($file->root()), $file->filename());
                        }
                    }
                    return $zip->close();
                }
            }
        }
    ],
    'fileMethods' => [
        'getWeight' => function () {
            return kirby()->site()->formatBytes($this->size());
        }
    ],
    'hooks' => [
        'system.loadPlugins:after' => function () {
            if (!option('silextaillenumerique.downloads-area.root')) {
                throw new Exception('"silextaillenumerique.downloads-area.root" config is not defined');
            }
            if (!site()->findPageOrDraft(option('silextaillenumerique.downloads-area.root'))) {
                kirby()->impersonate('kirby', function () {
                    Page::create([
                        'slug'      => option('silextaillenumerique.downloads-area.root'),
                        'template'  => 'da-home',
                        'content'   => [
                            'title'  => 'Téléchargements',
                            'loginIntro' => 'Bienvenue dans notre espace téléchargement. Merci de bien vouloir vous identifier.',
                            'signinBlockTitle' => 'Je souhaite obtenir un mot de passe',
                            'signinBlockText' => 'Merci de bien vouloir nous contacter à l\'adresse suivante : (email: contact@silextaillenumerique.email)',
                            'loginBlockTitle' => 'J\'ai un mot de passe',
                        ],
                        'draft' => false
                    ]);
                });
            }
        },
        'route:before' => function ($route, $path, $method) {
            if ($path === "panel" || substr($path, 0, 6) === "panel/") {
                if (kirby()->user() && kirby()->user()->role() == "da-guest") {
                    kirby()->user()->logout();
                    //go('login');
                }
            }
        },
        'page.update:after' => function ($newPage) {
            if ($newPage) {
                $newPage->setSectionArchive();
            }
        },
        'page.changeSlug:before' => function ($newPage) {
            if ($newPage) {
                $newPage->removeSectionArchive();
            }
        },
        'page.changeSlug:after' => function ($newPage) {
            if ($newPage) {
                $newPage->setSectionArchive();
            }
        },
        'page.duplicate:after' => function ($newPage) {
            if ($newPage) {
                $newPage->setSectionArchive();
            }
        },
        'page.delete:after' => function ($status, $page) {
            if ($status && $page) {
                $page->removeSectionArchive();
            }
        },
    ],
]);