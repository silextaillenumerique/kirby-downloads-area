
<nav id="da-navbar" style="background-color:<?=$site->topbarBackground()?>; color:<?=$site->topbarColor()?>;" >
		<a class="da-unstyled" href="<?= r($site->logolink()->isNotEmpty(), $site->logolink(), $site->url()) ?>">
			<?php if ($site->logo()->isNotEmpty() && $logo = $site->logo()->toFile()): ?>
				<img id="site-logo" src="<?= $logo->thumb(['width' => 360, 'height' => 120])->url() ?>" aria-label="logotype" aria-hidden="true">
			<?php else: ?>
				<span class="da-title--sm"><?= $site->title(); ?></span>
			<?php endif ?>
		</a>

	<?php if ($user = $kirby->user()): ?>
		<div class="da-user-widget">
			<span class="da-user-widget_message">Vous êtes connectés comme <strong><?= $user->name()->isNotEmpty() ? $user->name() : $user->username() ?></strong></span>
			<a class="da-unstyled da-button da-button--medium <?= r($site->topbarColor()->isNotEmpty() && $site->topbarColor()->upper() == '#FFF', 'da-button--luminous', 'da-button--transparent') ?>" href="<?= url('logout') ?>">
				<i class="da-icon"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
				  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
				</svg></i>
				<span>Se déconnecter</span>
			</a>
		</div>
	<?php endif ?>
</nav>