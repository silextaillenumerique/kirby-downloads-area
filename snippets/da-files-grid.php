<ul class="da-columns da-columns--large da-font-sans">
	<?php foreach ($files as $file): ?>
		<li class="da-grid-item da-col da-col-6 da-col-4--sm da-col-3--md da-col-2--lg">
			<a href="<?= $file->url() ?>" class="da-grid-item_link da-unstyled da-no-hover" target="_blank" download>
				<div class="da-grid-item_figure-container da-row--sm">
					<?php if ($file->type() == 'image'): ?>
						<figure class="da-grid-figure">
							<img src="<?= $file->thumb(['width' => 300, 'height' => 200])->url() ?>" alt="<?= $file->filename() ?>">
						</figure>
					<?php else: ?>
						<figure class="da-grid-icon">
							<div class="da-grid-icon__container">
								<div class="da-file-icon__wrapper">
									<div class="da-file-icon da-file-icon--lg" data-type="<?= $file->extension() ?>"></div>
								</div>
							</div>
						</figure>
					<?php endif ?>
				</div>


				<div class="da-grid-item_caption">
					<div class="da-text--sm da-row--xs"><span class="da-link-onhover da-hyphenated"><?= $file->filename() ?></span></div>
					<div class="da-row--xs da-font-mono da-text--sm da-text--gray">
						<?php if ($file->type() == 'image'): ?>
							<div><?= $file->dimensions() ?>&nbsp;px</div>
						<?php endif ?>
						<div><?= $file->getWeight() ?></div>
					</div>
					<?php if ($file->credits()->isNotEmpty()): ?>
						<div class="da-text--sm da-row--xs">
							<?= $file->credits() ?>
						</div>
					<?php endif ?>


				</div>
			</a>
		</li>
	<?php endforeach ?>
</ul>