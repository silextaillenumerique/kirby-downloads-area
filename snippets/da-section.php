

<?php $selection = $section->selection()->isNotEmpty() && $section->selection()->toFiles() ? $section->selection()->toFiles() : null ?>
<section id="<?= $section->uid() ?>" class="da-section da-row--xl">
	<form action="download" method="post" >
		<header class="da-section--header">
			<div class="da-section--header-content da-row--lg">
				<h3 class="da-section--title" ><a href="#<?= $section->uid() ?>" class="da-unstyled da-hash"><?= $section->title()->widont() ?></a></h3>
				<div class="da-section--header-button-container">
					<?php if ($selection && $section->getArchive()): ?>
						<button class="da-button da-button--medium" type="submit">
							<i class="da-icon"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
							  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7H5a2 2 0 00-2 2v9a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-3m-1 4l-3 3m0 0l-3-3m3 3V4" />
							</svg></i>
							<span>Télécharger <?= $selection->count() ?> <?= r($selection->count() > 1, 'fichiers', 'fichier') ?></span>
						</button>
					<?php endif ?>
				</div>
			</div>
			<?php if ($section->text()->isNotEmpty()): ?>
				<div class="da-text"><?= $section->text()->kt() ?></div>
			<?php endif ?>
		</header>
		<?php if ($selection): ?>
			<?php snippet('da-files-grid', ['files' => $selection]) ?>
		<?php else: ?>
			<div class="da-discret">Il n'y a pas de fichiers dans cette rubrique.</div>
		<?php endif ?>

		<input type="hidden" name="page" value="<?= $page->id() ?>"/>
		<input type="hidden" name="section" value="<?= $section->id() ?>"/>
	</form>

</section>