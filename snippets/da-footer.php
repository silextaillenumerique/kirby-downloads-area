        </div>
	</main>

	<?php snippet(option('silextaillenumerique.downloads-area.footer')) ?>

	<?php if (option('silextaillenumerique.downloads-area.custom-js')): ?>
		<?= js(option('silextaillenumerique.downloads-area.custom-js')) ?>
	<?php endif ?>

</body>
</html>
