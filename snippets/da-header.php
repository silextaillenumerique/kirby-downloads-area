<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    
    <?= css([
        'media/plugins/silextaillenumerique/downloads-area/css/reset.css',
        'media/plugins/silextaillenumerique/downloads-area/css/variables.css',
        'media/plugins/silextaillenumerique/downloads-area/css/typography.css',
        'media/plugins/silextaillenumerique/downloads-area/css/layout.css',
        'media/plugins/silextaillenumerique/downloads-area/css/inputs.css',
        'media/plugins/silextaillenumerique/downloads-area/css/elements.css',
    ]) ?>

    <?php if (option('silextaillenumerique.downloads-area.custom-css')): ?>
        <?= css(option('silextaillenumerique.downloads-area.custom-css')) ?>
    <?php endif ?>

    <style>
    	.material-icons{
    		visibility: hidden;
    	}
    </style>
</head>

<body>

    <?php snippet(option('silextaillenumerique.downloads-area.header')) ?>

    <main id="da-page-content" role="main">
        <div class="da-container">
            