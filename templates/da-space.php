<?php snippet('da-header') ?>

	<?php
	    $error = get('error') ?? false;
	    $error_type = get('error_type') ?? null;
	?>

  	<div id="da-space-page" class="da-block--xxl">
	    <section class="da-space-section">
	        <header class="da-space-section--header">
	          	<h2 class="da-row"><?= $page->title()->widont() ?></h2>
	          	<?php if ($error && $error_type): ?>
	          		<div class="da-color--error">Une erreur est survenue.</div>
	          	<?php endif ?>
	        </header>

	        <?php if ($page->text()->isNotEmpty()): ?>
	        	<div class="da-text da-text--xl">
	            	<?= $page->text()->kt() ?>
	        	</div>
	        <?php endif ?>
	    </section>

		<?php $sections = $page->children()->listed()->template('da-section') ?>
		<?php if ($sections->count()): ?>
			<!-- <h2 class="da-title--xs discret mb10 normal">Rubriques</h2> -->
	        <?php foreach ($sections as $section): ?>
	        	<?php snippet('da-section', ['section' => $section]) ?>
	        <?php endforeach ?>
		<?php else: ?>
			<div class="da-discret">Il n'y a pas de rubriques dans cet espace.</div>
		<?php endif ?>

	</div>

<?php snippet('da-footer') ?>
