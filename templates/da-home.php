<?php snippet('da-header') ?>

  	<div id="da-home-page" class="da-block--xxl">
	    <section class="da-home-page-header da-block--lg">
	        <?php if ($page->text()->isNotEmpty()): ?>
	          <div class="da-text da-text--xl da-block--xl">
	            <?= $page->text()->kt() ?>
	          </div>
	        <?php endif ?>
	    </section>

		<section class="">
			<?php foreach ($site->getUserSpaces() as $space): ?>
				<div class="da-row">
					<a class="da-space-link da-unstyled" href="<?= $space->url() ?>">
						<h2 class="flex">
							<span><?= $space->title() ?></span>
						</h2>
					</a>
				</div>
			<?php endforeach ?>
		</section>
	</div>

<?php snippet('da-footer') ?>
