<?php snippet('da-header') ?>

  <div id="da-login-page">
    <section class="da-login-header da-block--xxl">
        <?php if (page(option('silextaillenumerique.downloads-area.root'))->loginIntro()->isNotEmpty()): ?>
          <div class="da-text da-text--xl da-login-intro-block">
            <?= page(option('silextaillenumerique.downloads-area.root'))->loginIntro()->kt() ?>
          </div>
        <?php endif ?>
    </section>

    <div class="da-login-content da-block-xxl da-columns">
      <section class="da-col da-col-12 da-col-6--md">
        <div class="da-login-text-block">  
          <div class="da-row--lg"><h2 class="da-title--sm"><?= page(option('silextaillenumerique.downloads-area.root'))->signinBlockTitle()->widont() ?></h2></div>
          <div class="da-text">
            <?= page(option('silextaillenumerique.downloads-area.root'))->signinBlockText()->kt() ?>
          </div>
        </div>
      </section>

      <section class="da-col da-col-12 da-col-6--md">
        <div class="da-login-form-block">
          

          <header class="da-login-form-header"><h2 class="da-title--sm"><?= page(option('silextaillenumerique.downloads-area.root'))->loginBlockTitle()->widont() ?></h2></header>
          <div class="da-text">
            <?= page(option('silextaillenumerique.downloads-area.root'))->loginBlockText()->kt() ?>
          </div>

          <form method="post" class="da-font-sans">
            <?php $guests = $kirby->users()->filterBy('role', 'da-guest'); ?>
            <div class="da-user-field-wrapper da-row">
              <?php if ($guests->count() > 1): ?>
                <div>
                  <label for="user">Identifiant</label>
                  <select name="user" id="user" class="da-full" required>
                    <option value="" diasbled selected>Choisir un type d'accès</option>
                    <?php foreach ($guests as $guest): ?>
                      <option value="<?= $guest->id() ?>" <?= r($guest->id() == esc(get('id')), 'selected') ?> ><?= $guest->name() ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              <?php elseif ($guests->count() === 1 && $guest = $guests->first()): ?>
                <label class="da-row--md">
                  Se connecter avec un accès "<strong><?= $guest->name() ?></strong>"
                </label>
                <input type="hidden" id="user" name="user" value="<?=$guest->id()?>" >
              <?php else: ?>
                  <label for="user">Identifiant</label>
                  <input class="da-full <?= r($error_type == 'wrong_credentials', 'da-color--error') ?>" type="email" id="user" name="user" value="<?= esc(get('user')) ?>" placeholder="Votre identifiant" required>
              <?php endif ?>
            </div>

            <div class="da-password-field-wrapper da-row">
              <label for="password">Mot de passe</label>
              <input class="da-full <?= r($error_type == 'wrong_credentials', 'da-color--error') ?>" type="password" id="password" name="password" value="<?= esc(get('password')) ?>" placeholder="Le mot de passe que vous avez reçu" required>
            </div>

            <?php if ($error): ?>
              <div class="da-error-message-wrapper da-color-error da-row--ext">
                <?php if ($error_type == 'wrong_credentials'): ?>
                  Mot de passe invalide.
                <?php else: ?>
                  Une erreur est survenue.
                <?php endif ?>
              </div>
            <?php endif ?>

            <div class="da-submit-button-wrapper">
              <input class="da-button da-color-valid da-full" type="submit" name="login" value="S'identifier">
            </div>
          </form>

        </div>
      </section>
    </div>
  </div>

<?php snippet('da-footer') ?>